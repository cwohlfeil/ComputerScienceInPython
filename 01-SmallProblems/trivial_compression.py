class CompressedGene:
    """
    A CompressedGene is provided a str of characters representing the nucleotides in agene, 
    and it internally stores the sequence of nucleotides as a bit string. The __init__() 
    method’s main responsibility is to initialize the bit-string construct with the 
    appropriate data. __init__() calls _compress() to do the dirty work of actually 
    converting the provided str of nucleotides into a bit string.
    """
    def __init__(self, gene: str) -> None:    
        self._compress(gene)

    """
    If you start a method or instance variable name in a class with two leading underscores,
    Python will “name mangle” it, changing its implementationname with a salt and not making
    it easily discoverable by other classes.
    """
    def _compress(self, gene: str) -> None:    
        """
        The _compress() method looks at each character in the str of nucleotides sequentially. 
        When it sees an A, it adds 00 to the bit string. When it sees a C, it adds 01, and so on. 
        Remember that two bits are needed for each nucleotide. As a result, before we add each 
        new nucleotide, we shift the bit string two bits to the left (self.bit_string<<= 2).  
        
        Every nucleotide is added using an “or” operation (|). After the left shift, two 0s are 
        added to the right side of the bit string. In bitwise operations, “ORing” 
        (for example, self.bit_string |= 0b10) 0s with any other value results in the other 
        value replacing the 0s. In other words, we continually add two new bits to the right 
        side of the bitstring. The two bits that are added are determined by the type of the nucleotide.
        """
        self.bit_string: int = 1  # start with sentinel
        for nucleotide in gene.upper():        
            self.bit_string <<= 2  # shift left two bits
            if nucleotide == "A":  # change last two bits to 00
                self.bit_string |= 0b00
            elif nucleotide == "C":  # change last two bits to 01
                self.bit_string |= 0b01
            elif nucleotide == "G":  # change last two bits to 10
                self.bit_string |= 0b10
            elif nucleotide == "T":  # change last two bits to 11
                self.bit_string |= 0b11
            else:
                raise ValueError("InvalidNucleotide:{}".format(nucleotide))

    
    def decompress(self) -> str:    
        """
        decompress() reads two bits from the bit string at a time, and it uses those two bits to
        determine which character to add to the end of the str representation of the gene. Because 
        the bits are being read backward, compared to the order they were compressed in (right to 
        left instead of left to right), the str representation is ultimately reversed (using the 
        slicing notation for reversal [::-1]). Finally, note how the convenient int method 
        bit_length() aided in the development of decompress().
        """
        gene: str = ""
        for i in range(0, self.bit_string.bit_length() - 1, 2):  # - 1 to exclude sentinel
            bits: int = self.bit_string >> i & 0b11  # get just 2 relevant bits
            if bits == 0b00:  # A
                gene += "A"
            elif bits == 0b01:  # C
                gene += "C"
            elif bits == 0b10:  # G
                gene += "G"
            elif bits == 0b11:  # T
                gene += "T"
            else:
                raise ValueError("Invalid bits:{}".format(bits))
        return gene[::-1]  # [::-1] reverses string by slicing backward
        
    def __str__(self) -> str:  # string representation for pretty printing
        return self.decompress()


if __name__ == "__main__":
    # Use getsizeof to determine if compression is actually saving space  
    from sys import getsizeof    
    original: str = "TAGGGATTAACCGTTATATATATATAGCCATGGATCGATTATATAGGGATTAACCGTTATATATATATAGCCATGGATCGATTATA" * 100
    print("original is {} bytes".format(getsizeof(original)))
    compressed: CompressedGene = CompressedGene(original)  # compress
    print("compressed is {} bytes".format(getsizeof(compressed.bit_string)))
    print(compressed)  # decompress
    print("original and decompressed are the same:{}".format(
        original == compressed.decompress()))
