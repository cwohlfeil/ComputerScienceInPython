def fib2(n: int) -> int:
    # Returns 0 as the 0th number
    if n < 2: # base case
        return n
    return fib2(n - 2) + fib2(n - 1) # recursive case

if __name__ == "__main__":
    # Still a lot of calls
    print(fib2(5))
    print(fib2(10))